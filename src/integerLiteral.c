/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"

int
integerLiteral(CXCursor current __attribute__((unused)), CXCursor parent __attribute__((unused)), CXClientData data __attribute__((unused)))
{
	printf("Je suis en div IntegerLitteral\n");
	clang_visitChildren(current, visitor, 0);
	printf("Je sors de div IntegerLitteral\n");
	return (0);
}
