/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 */

#if defined(HAVE_CONFIG_H)
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

#include "adbg.h"
#include "customast.h"

static struct {
	int	cursorKing;
	int	(*callback)(CXCursor, CXCursor, CXClientData);
} cursors[] = {
	{ CXCursor_DeclRefExpr,		declRefExpr },
	{ CXCursor_BinaryOperator,	binaryOperator },
	{ CXCursor_FunctionDecl,	functionDecl },
	{ CXCursor_IntegerLiteral,	integerLiteral },
	{ -1,				defaultAction }
};

static CXTranslationUnit tu;

enum CXChildVisitResult
visitor(CXCursor current, CXCursor parent, CXClientData data)
{
	int i;
	enum CXCursorKind kind;
	CustomNode *node;
	CustomNode *old;

	old = getCurrent();
	kind = clang_getCursorKind(current);
	node = makeNode(getCurrent(), kind);
	if (node == NULL)
	{
		exit(1);
	}
	appendNode(node, &(getCurrent()->lhs));
	for (i = 0; cursors[i].cursorKing != -1; ++i) {
		if (cursors[i].cursorKing == kind)
			break;
	}
	/* cursors[i].callback(current, parent, data); */
	setCurrent(node);
	clang_visitChildren(current, visitor, 0);
	setCurrent(old);
	return (CXChildVisit_Continue);
}

int
main(int argc __attribute__((unused)), char **argv)
{
	CXIndex index;
	const char *filename;

	filename = argv[1];
	if (filename == NULL)
		return (2);
	setMotherOfAll(makeNode(NULL, -1));
	if (getMotherOfAll() == NULL)
	{
		return 1;
	}
	setCurrent(getMotherOfAll());
	index = clang_createIndex(0, 0);
	printf("toto\n");
	tu = clang_createTranslationUnitFromSourceFile(index, filename, 0, NULL,
						       0, 0);

	clang_visitChildren(clang_getTranslationUnitCursor(tu), visitor, 0);

	clang_disposeTranslationUnit(tu);
	clang_disposeIndex(index);
	printCustomAst(getMotherOfAll(), 0);
	return (0);
}
