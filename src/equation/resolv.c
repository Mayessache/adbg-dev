#include <stdlib.h>
#include <stdio.h>

#include "equation.h"
#include "node.h"
#include "operation.h"

static op_func		operation[5] =
  {
    &opp_add,
    &opp_sub,
    &opp_mul,
    &opp_mod,
    &opp_div
  };

t_node		*check_node(t_node *n)
{
  t_node *res;

  if (n->nodeType == EQUATION)
    {
      resolv((t_equa*)n->data, &res);
      free(n);
      return (res);
    }
  return (n);
}

int		intern_resolv(t_node *first, t_node *sec, t_node **res, t_OP op)
{
  t_node	*tmp = NULL;

  do {
    do {
      (*res) = operation[op](check_node(first), check_node(sec));
      (*res)->prev = tmp;
      (*res)->next = NULL;
      if (tmp != NULL)
	tmp->next = (*res);
      tmp = (*res);
    } while (sec->next != NULL && (sec = sec->next));
  } while (get_first_node(&sec) && first->next != NULL && (first = first->next));
  get_reduce_node(res);
  get_first_node(&first);
  release_node(first);
  release_node(sec);  
  return (0);
}

int		resolv(t_equa *e, t_node **res)
{
  intern_resolv(e->first, e->sec, res, e->operator);
  free(e);  
  return (0);
}
