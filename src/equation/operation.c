#include <stdlib.h>
#include <stdio.h>

#include "node.h"
#include "range.h"

/*
** Addition :
** [x1;x2] + [y1;y2] = [x1+y1;x2+y2]
**   
** Soustraction :
** [x1;x2] - [y1;y2] = [x1-y2;x2-y1]
** 
** Multiplication :
** [] * [] 
** 
** Division : ???
** 
*/

int		check_type(t_value **tab)
{
  int		tmp = 0;

  if (tab[1]->sym != NO)
    {
      tab[1] = tab[1] + 1;
      tmp++;
    }
  if (tab[2]->sym != NO)
    {
      tab[2] = tab[2] + 1;
      tmp++;
    }
  if (tmp != 0 && tab[0] != NULL)
    tab[0] = tab[0] + 1;
  return (tmp);
}

t_node		*create_node(t_node *v1, t_node *v2)
{
  t_node	*res;

  if ((res = malloc(sizeof(t_node))) == NULL)
    exit(0);
  if ((((t_value*)v1->data)->sym != NO) ||
       (((t_value*)v2->data)->sym != NO))
    {
      if ((res->data = malloc(sizeof(t_range))) == NULL)
	exit(0);
    }
  else if ((res->data = malloc(sizeof(t_value))) == NULL)
    exit(0);
  res->nodeType = VALUE;
  return (res);
}

t_value		*add_value(t_value *v1, t_value *v2, t_value *res)
{
  res->nbr = v1->nbr + v2->nbr;
  res->sym = (v1->sym == NO && v1->sym == NO) ? NO : (v1->sym + v2->sym) % 2;
  return (res);
}

t_node		*opp_add(t_node *v1, t_node *v2)
{
  t_node	*res;
  t_value	*tmp[3];

  res = create_node(v1, v2);
  tmp[0] = (t_value*)(res->data);
  tmp[1] = (t_value*)(v1->data);
  tmp[2] = (t_value*)(v2->data);
  tmp[0] = add_value(tmp[1], tmp[2], tmp[0]);
  if (check_type(tmp) != 0)
    tmp[0] = add_value(tmp[1], tmp[2], tmp[0]);
  return (res);
}

t_node		*opp_sub(t_node *v1, t_node *v2)
{
  t_node	*res;

  res = create_node(v1, v2);
  return (res);
}

t_node		*opp_mul(t_node *v1, t_node *v2)
{
  t_node	*res;

  res = create_node(v1, v2);
  return (res);
}

t_node		*opp_mod(t_node *v1, t_node *v2)
{
  t_node	*res;

  res = create_node(v1, v2);
  return (res);
}

t_node		*opp_div(t_node *v1, t_node *v2)
{
  t_node	*res;

  res = create_node(v1, v2);
  return (res);
}
