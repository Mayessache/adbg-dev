/*
 * Copyright (c) 2015, Quentin Schwerkolt
 * All rights reserved.
 *
 */

/*
** Modified On 25/02/2015
** By
** Mayessache
*/

#include <stdlib.h>
#include <stdio.h>

#include <clang-c/Index.h>
#include <clang-c/CXString.h>

static CXTranslationUnit tu;

enum CXChildVisitResult
visitor(CXCursor, CXCursor, CXClientData);


enum CXChildVisitResult
funcdecl(CXCursor cursor, CXCursor parent, CXClientData cdata)
{
  CXString str;

  str = clang_getCursorSpelling(cursor);
  printf("Function declaration: %s\n", clang_getCString(str));
  clang_disposeString(str);
  return (clang_visitChildren(cursor, visitor, 0));
}

enum CXChildVisitResult
otherFunc(CXCursor cursor, CXCursor parent, CXClientData cdata)
{
  return (clang_visitChildren(cursor, visitor, 0));
}

static struct {
  const int  id;
  enum CXChildVisitResult (*callback)(CXCursor, CXCursor, CXClientData);
  
} cmd[] = {
  { CXCursor_FunctionDecl, funcdecl},
  { -1, otherFunc}
};


enum CXChildVisitResult
visitor(CXCursor cursor, CXCursor parent, CXClientData cdata)
{
	CXString str1;
	CXToken *tokens, *tokens1;
	CXSourceRange range, range1;
	unsigned ntokens, ntokens1;
	char *s, *s1;


	CINDEX_LINKAGE enum CXCursorKind toto;

	int i = 0;
	toto = clang_getCursorKind(cursor);

	while (cmd[i].id != -1 && cmd[i].id != toto)
	  i++;

	cmd[i].callback(cursor, parent, cdata);

	return (CXChildVisit_Continue);
}

int
main(int argc, char **argv)
{
	CXIndex index;
	const char *filename;

	filename = argv[1];
	if (filename == NULL)
		return (2);
	index = clang_createIndex(0, 0);
	tu = clang_createTranslationUnitFromSourceFile(index, filename, 0, NULL,
						       0, 0);

	clang_visitChildren(clang_getTranslationUnitCursor(tu), visitor, 0);

	clang_disposeTranslationUnit(tu);
	clang_disposeIndex(index);
	return (0);
}
