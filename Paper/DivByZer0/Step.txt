Step 1 : Création de l'Equation
     Cette Equation correspond a un arbre de :
     	   Range_Value
	   Token
     Cet arbre est divisé en deux parties : Left Hand Side et Right Hand Side (LHS et RHS),
     correspondant aux parties à gauche et à droite de l'égalitée.
     Le RHS étant une Range_Value.
     - Creation d'une structure Equation
     - Ecritures de fonction de manipulation
     |_> ResolvEqu(Equation*)

Step 2 : Création d'une range de valeurs possible
     Cette Range_Value correspond a un conteneur.
     - Creation d'une Structure Range_Value
     - Ecriture de fonction de Manipulation
     |_> Create_Range()
     |_> PushToRange()
     |_> PopFromRange()
     |_> FindInRange(Range_Value*, int)
     |_> AddToRange(Range_Value*, Range_Value*)

Step 3 : Mise à jour de l'Equation ainsi que de Range_Value
     - Ecriture de fonction de mise a jour.

Step 4 : Evaluation de l'Equation
      // DeadLine 28 mars
Step 5 : Factorisation de l'Equation
      // Optimisation
Step 6 : Fancy maths (log, sqrt, etc...)
      // Bonus
