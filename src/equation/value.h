#ifndef __ADBG_VALUE_H__
#define __ADBG_VALUE_H__

typedef struct s_value t_value;

typedef enum	e_Sym
  {
    INCLUDE = 0,
    NONINCLUDE,
    NO,
    INF,
    NINF
  }		Sym;

struct		s_value
{
  enum e_Sym	sym;
  int		nbr;
};

#endif /* __ADBG_VALUE_H__*/
