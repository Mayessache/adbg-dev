#ifndef __ADBG_RANGE_H__
#define __ADBG_RANGE_H__

#include "value.h"

typedef struct s_range t_range;

struct		s_range
{
  t_value	v1;
  t_value	v2;
};

#endif /* __ADBG_RANGE_H__ */
