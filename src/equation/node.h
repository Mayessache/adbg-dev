#ifndef __ADBG_NODE_H__
#define __ADBG_NODE_H__

typedef struct s_node	t_node;

typedef enum	e_Node
  {
    VALUE = 0,
    EQUATION //,CURSORCLANG
  }		typeNode;


struct		s_node
{
  enum e_Node	nodeType;
  void		*data;
  t_node	*prev;
  t_node	*next;
};

int		get_first_node(t_node **);
int		release_node(t_node *);
int		get_reduce_node(t_node **n); //malloc free
t_node		*compare_value(t_node *, t_node **);

#endif /* __ADBG_NODE_H__ */
