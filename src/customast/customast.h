
#ifndef		_CUSTOMAST_H_
# define	_CUSTOMAST_H_

typedef struct CustomNode_s CustomNode;

struct CustomNode_s {
	CustomNode	*parent;
	CustomNode	*next;
	CustomNode	*lhs; // Left hand side
	CustomNode	*rhs; // Right hand side
	int		kind;
};

CustomNode *getMotherOfAll(void);
void setMotherOfAll(CustomNode *node);
CustomNode *getCurrent(void);
void setCurrent(CustomNode *node);
CustomNode *makeNode(CustomNode *parent, int kind);
void printIndent(int n);
void printCustomAst(CustomNode *node, int indent);
void appendNode(CustomNode *node, CustomNode **collection);

#endif

