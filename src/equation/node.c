#include <stdlib.h>

#include "node.h"

int		get_first_node(t_node **n)
{
  while ((*n) != NULL && (*n)->prev != NULL)
    (*n) = (*n)->prev;
  return (1);
}

int		release_node(t_node *n)
{
  t_node	*tmp;

  do {
    tmp = n->next;
    free(n->data);
    free(n);
  } while (tmp != NULL && (n = tmp));
  return (0);
}

int		get_reduce_node(t_node **n)
{
  t_node	*tmp;

  get_first_node(n);
  do {
    tmp = (*n)->next;
    while (tmp != NULL)
      tmp = compare_value(tmp, n);
  } while ((*n)->next != NULL && ((*n) = (*n)->next));
  get_first_node(n);
  return (0);
}
